package com.bipmit.release;

import com.bipmit.git.ReleaseGitOperations;
import com.bipmit.git.RepositoryOperations;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.junit.RepositoryTestCase;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.RepositoryCache;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.bipmit.git.GitOperationTestUtils.addAndCommitContent;
import static com.bipmit.git.GitOperationTestUtils.tag;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertThat;

public class PendingReleasesTest extends RepositoryTestCase {

    private Git git;

    private List<Repository> toClose = new ArrayList<>();

    PendingReleases pendingReleases;

    @Before
    public void setup() throws Exception {
        super.setUp();
        git = new Git(db);
        toClose.add(git.getRepository());

        String repoUri = fileUri();
        RepositoryOperations repositoryOperations = new RepositoryOperations(repoUri);
        ReleaseGitOperations releaseGitOperations = new ReleaseGitOperations(repositoryOperations);
        pendingReleases = new PendingReleases(releaseGitOperations);
    }

    @Test
    public void stagingReleases_shouldGetPendingStagingReleases() throws Exception {
        addAndCommitContent(db, git, "content-1.txt", "test-content1");
        tag(git, "alpha_release-1");
        tag(git, "alpha_staging-1");

        addAndCommitContent(db, git, "content-2.txt", "test-content2");
        tag(git, "alpha_release-2");
        tag(git, "alpha_staging-2");

        addAndCommitContent(db, git, "content-3.txt", "test-content3");
        tag(git, "alpha_release-3");

        addAndCommitContent(db, git, "content-4.txt", "test-content4");
        tag(git, "alpha_release-4");

        addAndCommitContent(db, git, "content-5.txt", "test-content5");
        tag(git, "alpha_release-5");

        assertThat(pendingReleases.stagingReleases(),
                hasItems(new Release("alpha_release-3"), new Release("alpha_release-4"), new Release("alpha_release-5")));
    }

    @Test
    public void productionReleases_shouldGetPendingProductionReleases() throws Exception {
        addAndCommitContent(db, git, "content-1.txt", "test-content1");
        tag(git, "alpha_release-1");
        tag(git, "alpha_staging-1");
        tag(git, "alpha_production-1");

        addAndCommitContent(db, git, "content-2.txt", "test-content2");
        tag(git, "alpha_release-2");
        tag(git, "alpha_staging-2");
        tag(git, "alpha_production-2");

        addAndCommitContent(db, git, "content-3.txt", "test-content3");
        tag(git, "alpha_release-3");
        tag(git, "alpha_staging-3");

        addAndCommitContent(db, git, "content-4.txt", "test-content4");
        tag(git, "alpha_release-4");
        tag(git, "alpha_staging-4");

        addAndCommitContent(db, git, "content-5.txt", "test-content5");
        tag(git, "alpha_release-5");

        assertThat(pendingReleases.productionReleases(),
                hasItems(new Release("alpha_staging-3"), new Release("alpha_staging-4")));

    }

    @After
    public void tearDown() throws Exception {
        RepositoryCache.clear();
        toClose.forEach(Repository::close);
        toClose.clear();
    }

    private String fileUri() {
        return "file://" + git.getRepository().getWorkTree().getAbsolutePath();
    }

}
