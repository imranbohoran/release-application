package com.bipmit.git;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.TagCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.revwalk.RevCommit;

import java.io.File;
import java.io.FileWriter;

public class GitOperationTestUtils {

    public static ObjectId addAndCommitContent(FileRepository db, Git git, String fileName, String content) throws Exception {
        File a = new File(db.getWorkTree(), fileName);
        touch(a, content);
        return git.commit().setAll(true).setMessage(content).call().getId();
    }

    public static ObjectId addAndCommitContentWithNotes(
        FileRepository db, Git git, String fileName, String commitContent, String... notes) throws Exception {
        File a = new File(db.getWorkTree(), fileName);
        touch(a, commitContent);
        RevCommit revCommit = git.commit().setAll(true).setMessage(commitContent).call();
        for(String note: notes) {
            git.notesAdd().setObjectId(revCommit).setMessage(note).call();
        }

        return revCommit.getId();
    }

    private static void touch(File f, String contents) throws Exception {
        FileWriter w = new FileWriter(f);
        w.write(contents);
        w.close();
    }


    public static void tag(Git git, String tag) throws GitAPIException {
        TagCommand tagCommand = git.tag().setName(tag)
                .setAnnotated(true);
        tagCommand.setMessage(tag);
        tagCommand.call();
    }

}
