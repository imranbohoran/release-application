package com.bipmit.git;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.junit.RepositoryTestCase;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.RepositoryCache;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.bipmit.git.GitOperationTestUtils.addAndCommitContent;
import static com.bipmit.git.GitOperationTestUtils.tag;
import static com.bipmit.git.ReleaseGitOperations.EMPTY_STRING;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class ReleaseGitOperationsTest extends RepositoryTestCase {
    private Git git;

    private List<Repository> toClose = new ArrayList<>();

    ReleaseGitOperations releaseGitOperations;

    @Before
    public void setup() throws Exception {
        super.setUp();

        git = new Git(db);
        toClose.add(git.getRepository());

        String repoUri = fileUri();
        RepositoryOperations repositoryOperations = new RepositoryOperations(repoUri);
        releaseGitOperations = new ReleaseGitOperations(repositoryOperations);
    }

    @Test
    public void shouldGetTagForGivenPrefix() throws Exception {
        addAndCommitContent(db, git, "content-1.txt", "test-content1");
        tag(git, "alpha_release-1");
        tag(git, "alpha_staging-1");

        addAndCommitContent(db, git, "content-2.txt", "test-content2");
        tag(git, "alpha_release-2");

        addAndCommitContent(db, git, "content-3.txt", "test-content3");
        tag(git, "alpha_release-3");
        tag(git, "alpha_release-4");
        tag(git, "alpha_release-5");

        String tagName = releaseGitOperations.getLatestTagForPrefix("alpha_release");

        assertThat(tagName, is("alpha_release-5"));

        String stagingTagName = releaseGitOperations.getLatestTagForPrefix("alpha_staging");
        assertThat(stagingTagName, is("alpha_staging-1"));
    }

    @Test
    public void shouldGetCommitForTag() throws Exception {
        ObjectId commit1 = addAndCommitContent(db, git, "content-1.txt", "test-content1");

        tag(git, "alpha_release-1");
        tag(git, "alpha_staging-1");

        ObjectId commit2 = addAndCommitContent(db, git, "content-2.txt", "test-content2");
        tag(git, "alpha_release-2");

        String expectedCommit1_for_release = releaseGitOperations.getCommitForTag("alpha_release-1");
        String expectedCommit1_for_staging = releaseGitOperations.getCommitForTag("alpha_staging-1");
        String expectedCommit2 = releaseGitOperations.getCommitForTag("alpha_release-2");

        assertThat(expectedCommit1_for_release, is(commit1.name()));
        assertThat(expectedCommit1_for_staging, is(commit1.name()));
        assertThat(expectedCommit2, is(commit2.name()));
    }

    @Test
    public void shouldGetLatestTagForCommit() throws Exception {
        ObjectId objectIdCommit1 = addAndCommitContent(db, git, "content-1.txt", "test-content1");
        tag(git, "alpha_release-1");
        tag(git, "alpha_staging-1");
        tag(git, "alpha_release-2");

        ObjectId objectIdCommit2 = addAndCommitContent(db, git, "content-2.txt", "test-content2");
        tag(git, "alpha_release-3");

        ObjectId objectIdCommit3 = addAndCommitContent(db, git, "content-3.txt", "test-content3");
        tag(git, "alpha_release-4");
        tag(git, "alpha_staging-4");
        tag(git, "alpha_staging-5");

        String tagNameForCommit1 = releaseGitOperations.getLatestTypedTagForCommit("alpha_release", objectIdCommit1.name());
        String tagNameForCommit1Staging = releaseGitOperations.getLatestTypedTagForCommit("alpha_staging", objectIdCommit1.name());
        String tagNameForCommit2 = releaseGitOperations.getLatestTypedTagForCommit("alpha_release", objectIdCommit2.name());
        String tagNameForCommit2Staging = releaseGitOperations.getLatestTypedTagForCommit("alpha_staging", objectIdCommit2.name());
        String tagNameForCommit3 = releaseGitOperations.getLatestTypedTagForCommit("alpha_staging", objectIdCommit3.name());

        assertThat(tagNameForCommit1, is("alpha_release-2"));
        assertThat(tagNameForCommit1Staging, is("alpha_staging-1"));
        assertThat(tagNameForCommit2, is("alpha_release-3"));
        assertThat(tagNameForCommit2Staging, is(EMPTY_STRING));
        assertThat(tagNameForCommit3, is("alpha_staging-5"));
    }

    @Test
    public void shouldListAllTagsSinceGivenTag() throws Exception {
        addAndCommitContent(db, git, "content-1.txt", "test-content1");
        tag(git, "alpha_release-1");

        addAndCommitContent(db, git, "content-2.txt", "test-content2");
        tag(git, "alpha_release-2");

        addAndCommitContent(db, git, "content-3.txt", "test-content3");
        tag(git, "alpha_release-3");

        addAndCommitContent(db, git, "content-4.txt", "test-content4");
        tag(git, "alpha_release-4");

        addAndCommitContent(db, git, "content-5.txt", "test-content5");
        tag(git, "alpha_release-5");

        List<String> alpha_release = releaseGitOperations.getTagsSinceTagWithPrefix("alpha_release", "alpha_release-2");

        assertThat(alpha_release, hasItems("alpha_release-5", "alpha_release-4", "alpha_release-3"));

    }

    @After
    public void tearDown() throws Exception {
        RepositoryCache.clear();
        toClose.forEach(Repository::close);
        toClose.clear();
    }

    private String fileUri() {
        return "file://" + git.getRepository().getWorkTree().getAbsolutePath();
    }

}
