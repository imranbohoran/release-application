package com.bipmit.git;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.junit.RepositoryTestCase;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.RepositoryCache;
import org.eclipse.jgit.revwalk.RevCommit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.bipmit.git.GitOperationTestUtils.addAndCommitContent;
import static com.bipmit.git.GitOperationTestUtils.addAndCommitContentWithNotes;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class RepositoryOperationsTest extends RepositoryTestCase {

    private Git git;
    private List<Repository> toClose = new ArrayList<>();

    @Before
    public void setup() throws Exception {
        super.setUp();

        git = new Git(db);
        toClose.add(git.getRepository());
    }

    @Test
    public void ensureRepositoryOperationsInitialisesWithRepository() throws IOException, GitAPIException {
        String repoUri = fileUri();
        RepositoryOperations repositoryOperations = new RepositoryOperations(repoUri);

        assertNotNull(repositoryOperations.getGit().getRepository());
        toClose.add(repositoryOperations.getGit().getRepository());
    }

    @Test
    public void ensureRepositoryOperationsCanBeInitialisedWithRemoteRepository() {
        String repoUri = remoteUriSSH();
        RepositoryOperations repositoryOperations = new RepositoryOperations(repoUri, true);

        assertNotNull(repositoryOperations.getGit().getRepository());
        toClose.add(repositoryOperations.getGit().getRepository());
    }


    @Test
    public void pull_shouldPullLatestChanges() throws Exception {
        ObjectId commit1 = addAndCommitContent(db, git, "content-1.txt", "test-content1");
        System.out.println("Commit 1 is : "+ commit1.name());

        String repoUri = fileUri();
        RepositoryOperations repositoryOperations = new RepositoryOperations(repoUri);

        ObjectId commit2 = addAndCommitContent(db, git, "content-2.txt", "test-content2");
        ObjectId commit3 = addAndCommitContent(db, git, "content-3.txt", "test-content3");

        repositoryOperations.pull();

        Iterable<RevCommit> revCommits = repositoryOperations.getGit().log().call();
        List<RevCommit> revCommitList = StreamSupport.stream(Spliterators.spliteratorUnknownSize(revCommits.iterator(), Spliterator.ORDERED), false)
                .collect(Collectors.toList());

        assertThat(revCommitList.size(), is(3));
        assertThat(revCommitList.get(0).name(), is(commit3.name()));
        assertThat(revCommitList.get(1).name(), is(commit2.name()));
        assertThat(revCommitList.get(2).name(), is(commit1.name()));
    }

    @Test
    public void pull_shouldPullChangesFromRemoteRepository() throws GitAPIException {
        String repoUri = remoteUriSSH();
        RepositoryOperations repositoryOperations = new RepositoryOperations(repoUri, true);
        repositoryOperations.pull();

        Iterable<RevCommit> revCommits = repositoryOperations.getGit().log().call();

        List<RevCommit> revCommitList = StreamSupport.stream(Spliterators.spliteratorUnknownSize(revCommits.iterator(), Spliterator.ORDERED), false)
                .collect(Collectors.toList());

        assertTrue(revCommitList.size() > 0);
    }

    @Test
    public void getRevCommit_shouldGetTheRevCommitForGivenCommit() throws Exception {
        ObjectId commit1 = addAndCommitContent(db, git, "content-1.txt", "test-content1");
        ObjectId commit2 = addAndCommitContent(db, git, "content-2.txt", "test-content2");
        ObjectId commit3 = addAndCommitContent(db, git, "content-3.txt", "test-content3");

        String repoUri = fileUri();
        RepositoryOperations repositoryOperations = new RepositoryOperations(repoUri);

        assertThat(commit1.name(), is(repositoryOperations.getRevCommit(commit1).name()));
        assertThat(commit2.name(), is(repositoryOperations.getRevCommit(commit2).name()));
        assertThat(commit3.name(), is(repositoryOperations.getRevCommit(commit3).name()));
    }

    @Test
    public void fetchNotes_shouldFetchAllNotes() throws Exception {
        String noteForCommit1 = "test-note-who=bob marley";
        ObjectId objectId1 = addAndCommitContentWithNotes(db, git, "content-notes-1.txt",
                "test-content-notes", "test-note=note_1_created", noteForCommit1);
        String noteForCommit2 = "test-note-who=bob dylan";
        ObjectId objectId2 = addAndCommitContentWithNotes(db, git, "content-notes-1.txt",
                "test-content-notes", "test-note=note_1_created", noteForCommit2);
        String noteForCommit3 = "test-note-who=james brown";
        ObjectId objectId3 = addAndCommitContentWithNotes(db, git, "content-notes-1.txt",
                "test-content-notes", "test-note=note_1_created", noteForCommit3);

        String repoUri = fileUri();
        RepositoryOperations repositoryOperations = new RepositoryOperations(repoUri);

        assertThat(repositoryOperations.fetchNotesForCommit(objectId1), is(noteForCommit1));
        assertThat(repositoryOperations.fetchNotesForCommit(objectId2), is(noteForCommit2));
        assertThat(repositoryOperations.fetchNotesForCommit(objectId3), is(noteForCommit3));
    }

    @After
    public void tearDown() throws Exception {
        RepositoryCache.clear();
        toClose.forEach(Repository::close);
        toClose.clear();
    }

    private String fileUri() {
        return "file://" + git.getRepository().getWorkTree().getAbsolutePath();
    }

    private String remoteUriHttps() {
        return "https://imranbohoran@bitbucket.org/imranbohoran/java-8-sandbox.git";
    }

    private String remoteUriSSH() {
        return "git@bitbucket.org:imranbohoran/release-application.git";
    }
}
