package com.bipmit.release;

import com.bipmit.git.ReleaseGitOperations;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class PendingReleases {

    private Logger logger = LoggerFactory.getLogger(PendingReleases.class);
    private final ReleaseGitOperations releaseGitOperations;

    public PendingReleases(ReleaseGitOperations releaseGitOperations) {
        this.releaseGitOperations = releaseGitOperations;
    }

    public List<Release> stagingReleases() {
        return pendingReleaseFor("alpha_staging", "alpha_release");
    }

    public List<Release> productionReleases() {
        return pendingReleaseFor("alpha_production", "alpha_staging");
    }

    private List<Release> pendingReleaseFor(String currentType, String requiredType) {
        try {
            String currentTypeLatestTag = releaseGitOperations.getLatestTagForPrefix(currentType);
            logger.info("The current "+ currentType + " tag is : "+ currentTypeLatestTag);

            String commitForCurrentTypeTag = releaseGitOperations.getCommitForTag(currentTypeLatestTag);
            logger.info("The current "+ currentType +" tag is on commit : "+ commitForCurrentTypeTag);
            String requeidTypeOnCurrentTypeEnv = releaseGitOperations.getLatestTypedTagForCommit(requiredType, commitForCurrentTypeTag);

            return releaseGitOperations.getTagsSinceTagWithPrefix(requiredType, requeidTypeOnCurrentTypeEnv).stream()
                    .map(Release::new)
                    .collect(Collectors.toList());
        } catch (GitAPIException | IOException e) {
            throw new ReleaseRuntimeException(e);
        }
    }
}
