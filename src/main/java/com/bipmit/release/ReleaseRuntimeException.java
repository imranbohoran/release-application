package com.bipmit.release;


public class ReleaseRuntimeException extends RuntimeException {

    public ReleaseRuntimeException(Throwable cause) {
        super(cause);
    }
}
