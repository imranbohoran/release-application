package com.bipmit.git;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.eclipse.jgit.lib.Constants.R_TAGS;

public class ReleaseGitOperations {

    static final String EMPTY_STRING = "";
    private RepositoryOperations repositoryOperations;

    public ReleaseGitOperations(RepositoryOperations repositoryOperations) throws IOException, GitAPIException {
        this.repositoryOperations = repositoryOperations;
    }

    public String getLatestTagForPrefix(String prefix) throws GitAPIException, IOException {
        repositoryOperations.pull();
        Collection<Ref> refs = repositoryOperations.getGit().getRepository().getRefDatabase().getRefs(R_TAGS).values();

        return refs.stream()
                .filter(ref -> ref.getName().startsWith(R_TAGS + prefix))
                .reduce((ref1, ref2) -> ref2)
                .map(ref -> ref.getName().split("/")[2])
                .orElse(EMPTY_STRING);
    }

    public String getCommitForTag(String tag) throws IOException {
        repositoryOperations.pull();
        Collection<Ref> refs = repositoryOperations.getGit().getRepository().getRefDatabase().getRefs(R_TAGS).values();
        return refs.stream()
                .filter(ref -> ref.getName().equals(R_TAGS + tag))
                .findFirst()
                .map(ref -> repositoryOperations.getRevCommit(ref.getObjectId()).name())
                .orElse(EMPTY_STRING);
    }

    public String getLatestTypedTagForCommit(String tagType, String commit) throws IOException {
        repositoryOperations.pull();
        Collection<Ref> refs = repositoryOperations.getGit().getRepository().getRefDatabase().getRefs(R_TAGS).values();

        List<Ref> refListForTagType = refs.stream()
                .filter(ref -> ref.getName().startsWith(R_TAGS + tagType))
                .collect(Collectors.toList());

        Collections.reverse(refListForTagType);

        return refListForTagType.stream()
                .filter(ref -> commit.equals(repositoryOperations.getRevCommit(ref.getObjectId()).name()))
                .findFirst()
                .map(ref -> ref.getName().split("/")[2])
                .orElse(EMPTY_STRING);

    }

    public String getMergeCommitsInRangeWithMatchingTagPrefix(String fromTag, String toTag, String tagPrefix) {
        return null;
    }

    public String getLastMergeCommitFromTagWithMatchingTagPrefix(String tagPrefix, String tag) {
        return null;
    }

    public String showDiff(String fromTag, String toTag) {
        return null;
    }

    public boolean isTagExists(String tagName) {
        return false;
    }

    public boolean isNotesAvailable(String commit) {
        return false;
    }

    public String getNoteForCommitWithKeyword(String commit, String keyword) {
        return null;
    }

    public String getReleaseTagForCommit(String commit) {
        return null;
    }

    public void pushNotes() {

    }

    public void appendNote(String commit, String note) {

    }

    public List<String> getMatchingTagsInCreationOrder(String tagPattern) {
        return null;
    }

    public boolean isTagSignedWithAuthorisedKey(String tagName, String authorisedApprovers) {
        return false;
    }


    public List<String> getTagsSinceTagWithPrefix(String prefix, String sinceTagName) throws IOException {
        repositoryOperations.pull();
        Collection<Ref> refs = repositoryOperations.getGit().getRepository().getRefDatabase().getRefs(R_TAGS).values();

        List<String> tagList = refs.stream()
                .filter(ref -> ref.getName().startsWith(R_TAGS + prefix))
                .map(ref -> ref.getName().split("/")[2])
                .collect(Collectors.toList());

        List<String> removeList = new ArrayList<>();
        for(String taggedBefore: tagList) {
            if(sinceTagName.equals(taggedBefore)) {
                break;
            }
            removeList.add(taggedBefore);
        }

        tagList.removeAll(removeList);

        return tagList;
    }
}
