package com.bipmit.git;

import com.bipmit.release.ReleaseRuntimeException;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.TransportConfigCallback;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.notes.Note;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.OpenSshConfig;
import org.eclipse.jgit.transport.RefSpec;
import org.eclipse.jgit.transport.SshTransport;
import org.eclipse.jgit.util.FS;
import org.eclipse.jgit.util.FileUtils;

import java.io.File;
import java.io.IOException;

public class RepositoryOperations {

    private Git git;
    private File tmp;
    private boolean remote;
    JschConfigSessionFactory sessionFactory;
    TransportConfigCallback transportConfigCallback;

    public RepositoryOperations(String repoUri) {
        this(repoUri, false);
    }

    public RepositoryOperations(String repoUri, boolean remote) {
        this.remote = remote;
        this.sessionFactory = new JschConfigSessionFactory() {
            @Override
            protected void configure(OpenSshConfig.Host hc, Session session) {
                session.setConfig("StrictHostKeyChecking", "no");
            }

            @Override
            protected JSch createDefaultJSch(FS fs) throws JSchException {
                JSch defaultJSch = super.createDefaultJSch(fs);
                defaultJSch.addIdentity("/Users/imranbohoran/.ssh/imran-bb");
                return defaultJSch;
            }

        };

        this.transportConfigCallback = transport -> {
            SshTransport sshTransport = (SshTransport) transport;
            sshTransport.setSshSessionFactory(sessionFactory);
        };

        try {
            File directory = createTempDirectory("testCloneRepository");
            CloneCommand command = Git.cloneRepository();
            command.setDirectory(directory);
            command.setURI(repoUri);
            if(remote) {
                command.setTransportConfigCallback(transportConfigCallback);
            }
            this.git = command.call();
        } catch (IOException | GitAPIException e) {
            throw new ReleaseRuntimeException(e);
        }
    }

    public Git getGit() {
        return this.git;
    }

    public void pull() {
        try {
            PullCommand pullCommand = this.getGit().pull();
            if(remote) {
                pullCommand.setTransportConfigCallback(transportConfigCallback);
            }
            pullCommand.call();
        } catch (GitAPIException e) {
            throw new ReleaseRuntimeException(e);
        }
    }

    public RevCommit getRevCommit(ObjectId commitId) {
        pull();
        RevWalk revWalk = new RevWalk(git.getRepository());

        try {
            return revWalk.parseCommit(commitId);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String fetchNotesForCommit(ObjectId commit) throws GitAPIException, IOException {
        RefSpec notesSpec = new RefSpec("refs/notes/*:refs/notes/*");
        git.fetch().setRefSpecs(notesSpec).call();
        Note noteForCommit = git.notesShow().setObjectId(getRevCommit(commit)).call();
        String notes = new String(git.getRepository().open(noteForCommit.getData()).getCachedBytes());
        return notes;
    }

    private File createTempFile() throws IOException {
        File p = File.createTempFile("tmp_", "", tmp);
        if (!p.delete()) {
            throw new IOException("Cannot obtain unique path " + tmp);
        }
        return p;
    }

    private File createTempDirectory(String name) throws IOException {
        File directory = new File(createTempFile(), name);
        FileUtils.mkdirs(directory);
        return directory.getCanonicalFile();
    }

}
